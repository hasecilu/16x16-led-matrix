External resources used in this project

1. LED Matrix library 
	https://github.com/aniline/akn-kicad-libs
	
	git clone https://github.com/aniline/akn-kicad-libs
	On KiCad:
		Open Symbol Editor (Ctrl+L)
			Preferences>Manage Symbol Libraries
			Go to Project Specific Libraries
			Add existing library to table (folder icon)
			Select files
			CLic OK
