# 16x16 LED matrix

This is a simple PCB made in KiCad using 4 small 8x8 LED matrices serially connected to form a 16x16 LED matrix.

Designed with [KiCad](https://kicad.org/download/)

[Download here](https://gitlab.com/hasecilu/16x16-led-matrix/-/archive/master/16x16-led-matrix-master.zip)

## Pictures

First render
![KiCad render: front](Front.png)

Second render
![KiCad render: back](Back.png)

At the moment I don't have the PCB, picture in the future.
